<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvoiceLine extends Model
{
    protected $fillable = [
        'invoice_id','description','created_at', 'amount'
    ];

    public function getUpdatedAtColumn() {
        return null;
    }
}
