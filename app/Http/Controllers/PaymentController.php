<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Redirect,Response;
use Auth;

//Additionals
use App\Invoice;
use App\InvoiceLine;
use App\Customer;
use App\Payment;

class PaymentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {            
        $data['payments'] = Payment::orderBy('id','desc')

        ->join('customers', 'payments.customer_id', '=', 'customers.id')
        ->select('customers.name', 'payments.*')
        ->get();
       
        return view('payments',$data);
    }
    
    public function store(Request $request)
    {  
        if ($request->customerid !== 'Select Customer'){        
            $data = Invoice::updateOrCreate(
                ['id' => $request->task_id],
                [   
                      'customer_id' => $request->customerid
                    , 'description' => $request->description
                    , 'created_at'  => now()
                    , 'amount'      => $request->amount
                ]
                                        
            );
        }

        elseif ($request->invoiceid !== null){ 

            $data = InvoiceLine::updateOrCreate(
                ['id' => $request->task_id],
                [   
                      'invoice_id'  => $request->invoiceid,
                      'description' => $request->description,
                      'created_at'  => now(),
                      'amount'      => $request->amount
                ]                                        
            );

            $newAmount = $request->amount + $request->oldamount;

            $data = Invoice::updateOrCreate(
                ['id' => $request->invoiceid],
                [
                  'amount'  => $newAmount
                ]                                        
            );

        }

        else { 
            
            $customerBalance = Customer::where('id', '=', $request->oldamount)
                                        ->select('customers.balance')
                                        ->get(); 
            // echo($customerBalance[0]['balance']);

            if($request->amount <= $customerBalance[0]['balance'] ){

                $data = Payment::updateOrCreate(
                    [   
                          'customer_id' => $request->oldamount
                        , 'created_at'  => now()
                        , 'amount'      => $request->amount
                    ]                                            
                );

                $newAmount = $customerBalance[0]['balance']-$request->amount;
                
                $data = Customer::updateOrCreate(
                    ['id' => $request->oldamount],
                    [
                    'balance'  => $newAmount
                    ]                                        
                );

                $data = "Payment Successfull";

            }
            else{
                $data = "Insuficient Funds";
            }

            
        }

        return Response::json($data);
    }
    
    public function edit($id)
    {   
        
    }

}
