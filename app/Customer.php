<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{    
    protected $fillable = [
        'name','address','username','password', 'balance'
    ];

    public function getUpdatedAtColumn() {
        return null;
    }
}
