<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

//Additionals
use App\Customer;
use Redirect,Response;

class CustomerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
            
        $data['customers'] = Customer::orderBy('id','desc')->paginate(8);        
        return view('customers',$data);
    }
    
    public function store(Request $request)
    {  
        $user = Customer::updateOrCreate(
            ['id' => $request->user_id],
            [   
                'name' => $request->name, 
                'address' => $request->address,
                'created_at' => now(),
                'username' => $request->username,
                'password' => $request->password,
                'balance' => $request->balance,
            ]
        );
        return Response::json($user);
    }
    
    public function edit($id)
    {   
        $where = array('id' => $id);
        $user  = Customer::where($where)->first();
 
        return Response::json($user);
    }
 
    public function destroy($id)
    {
        $user = Customer::where('id',$id)->delete();
   
        return Response::json($user);
    }
    
}
