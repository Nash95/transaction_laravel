<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//Customers
Route::resource('customers', 'CustomerController');

//Invoices
Route::resource('invoices', 'InvoiceController');

//Payments
Route::resource('payments', 'PaymentController');
