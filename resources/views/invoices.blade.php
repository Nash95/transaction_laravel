@extends('layouts.admin')

@section('content')
    
<div class="container-fluid">
    <h2 style="margin-top: 12px;" class="alert alert-success">Invoices</h2><br>
    <div class="row">
        <div class="col-12">
        <a href="javascript:void(0)" class="btn btn-success mb-2" id="create-new-invoice">Create Invoice</a> 
          
        <div class="card">

          <div class="card-body table-responsive p-0" style="height: 678px;">
          <!-- Table -->
            <div id="userstable">
            @include('invoiceTable')
            </div>
          <!-- Table End -->
          
          </div>
        </div>  
       </div> 
    </div>
</div>

<div class="modal fade" id="task-modal" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title" id="taskModal"></h4>
        </div>
        <form id="userForm" name="userForm" class="form-horizontal">
          <div class="modal-body">

              <div class="form-group" id ="assigneeTag">   
                <label class="col-sm-2 control-label">Customer</label> 
                <div class="col-sm-12">
                  <select class="form-control" name="customerid" id="customerid">
                    <option>Select Customer</option>

                    @foreach ($customers as $user)
                      <option value="{{ $user->id }}" > {{ $user->name }} </option>
                    @endforeach    
                  </select>
                </div>
              </div>

              <div class="form-group" id="invoiceID">
                <label class="col-sm-2 control-label">InvoiceID</label>
                <div class="col-sm-12">
                    <input type="number" class="form-control" id="invoiceid" name="invoiceid" value="" required="" disabled>
                </div>
              </div>

              <div class="form-group" id="customerPayName">
                <label class="col-sm-2 control-label">Customer Name</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control" id="customername" name="customername" value="" required="" disabled>
                </div>
              </div>

              <div class="form-group" id="oldAMT">
                <div class="col-sm-12">
                    <input type="number" class="form-control" id="oldamount" name="oldamount" value="" required="" hidden>
                </div>
              </div>
              
              <div class="form-group" id="Description">
                <label class="col-sm-2 control-label">Description</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control" id="description" name="description" placeholder="Enter Description" value="" maxlength="50" required="">
                </div>
              </div>
 
              <div class="form-group">
              <label class="col-sm-2 control-label">Amount</label>
                <div class="col-sm-12">
                    <input type="number" class="form-control" id="amount" name="amount" placeholder="Enter Amount" value="" required="">
                </div>
              </div>

              <div class="form-group">
                <div class="col-sm-12">
                    <input type="text" class="form-control" id="taskStatus" name="taskStatus" value="0" hidden>
                </div>
              </div>

          </div>
          <div class="modal-footer">
              <button type="submit" class="btn btn-primary" id="btn-save" value="create">
                Create
              </button>
          </div>
        </form>
    </div>
  </div>
</div>


<script>
  $(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    /*  When user click add user button */
    $('#create-new-invoice').click(function () {        
      $('#btn-save').val("create-Invoice");
      $('#userForm').trigger("reset");
      $('#taskModal').html("Create Invoice");
      $('#invoiceID').fadeOut();
      $('#customerPayName').fadeOut();
      $('#assigneeTag').fadeIn();
      $('#Description').fadeIn(); 
      $('#task-modal').modal('show');

    });
    /*  End When user click add user button */

    /*  When user click add invoiceline button */
    $('body').on('click', '#create-new-invoiceLine', function () {

      var invoice_id = $(this).data('id');
      var old_amount = $(this).data('name');
      $('#btn-save').val("create-InvoiceLine");
      $('#userForm').trigger("reset");
      $('#taskModal').html("Create InvoiceLine");
      $('#assigneeTag').fadeOut();
      $('#customerPayName').fadeOut();
      $('#invoiceID').fadeIn();
      $('#Description').fadeIn();        
      $('#task-modal').modal('show');
      $('#invoiceid').val(invoice_id);
      $('#oldamount').val(old_amount);
        
    }); 
    /*End When user click add invoiceline button */

    /*  When user click make-payment button */
    $('body').on('click', '#make-payment', function () {

      var customer_pay_id = $(this).data('id');
      var customer_name = $(this).data('name');
      $('#btn-save').val("create-Payment");
      $('#userForm').trigger("reset");
      $('#taskModal').html("Make Payment");

      $('#assigneeTag').fadeOut();
      $('#invoiceID').fadeOut();
      $('#Description').fadeOut();
      $('#customerPayName').fadeIn();

      $('#task-modal').modal('show');
      $('#customername').val(customer_name);
      $('#oldamount').val(customer_pay_id);

    }); 
    /*End When user click make-payment button*/ 


  });
 
if  ($("#userForm").length > 0) {
      $("#userForm").validate({
  
        submitHandler: function(form) {
      
          var actionType = $('#btn-save').val();
          $('#btn-save').html('Sending..');
          
          $.ajax({
              data: $('#userForm').serialize(),
              url: "{{ url('invoices')}}", 
              type: "POST",
              dataType: 'json',
              success: function (data) {

                  console.log(data);
                  var user = '<tr id="task_id_' + data.id + '"><td>' + data.id + '</td><td>' + data.customer_id + '</td><td>' + data.description + '</td><td>' + data.created_at + '</td><td>' + data.amount + '</td>';
                      user += '<td><a href="javascript:void(0)" id="create-new-invoiceLine" data-id="' + data.id + '" class="btn btn-success toChat">Add</a></td>';
                      user += '<td><a href="javascript:void(0)" id="make-payment" data-id="' + data.id + '" class="btn btn-info toEdit">Pay</a></td></tr>';
                
                  if (data == "Payment Successfull"){  
                    alert(data);
                  }  

                  if (data == "Insuficient Funds"){       
                    alert(data);
                  }  
                  
                  
                  if (actionType == "create-Invoice") {
                      location.reload(); 
                      // $('#users-crud').prepend(user);
                  } 
                  else if(actionType == "create-InvoiceLine"){
                      $("#task_id_" + data.id).replaceWith(user);
                  }
      
                  $('#userForm').trigger("reset");
                  $('#task-modal').modal('hide');
                  $('#btn-save').html('Create');
                  
            },
            error: function (data) {
                console.log('Error:', data);
                $('#btn-save').html('Save Changes');
            }
            
          });
        }
      })
    }
   
  
</script>
    
@endsection