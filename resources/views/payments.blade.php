@extends('layouts.admin')

@section('content')
    
<div class="container-fluid">
    <h2 style="margin-top: 12px;" class="alert alert-success">Payments</h2><br>
    <div class="row">
        <div class="col-12">
          
        <div class="card">

          <div class="card-body table-responsive p-0" style="height: 678px;">
          <!-- Table -->
            <div id="userstable">
            <table class="table table-head-fixed text-nowrap" id="laravel_crud">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>CustomerName</th>
                        <th>Date Created</th>
                        <th>Amount</th>
                        
                    </tr>
                </thead>
                <tbody id="users-crud">
                    @foreach($payments as $t_info)
                    <tr >
                        <td>{{ $t_info->id  }}</td>
                        <td>{{ $t_info->name }}</td>
                        <td>{{ $t_info->created_at }}</td>
                        <td>{{ $t_info->amount }}</td>
                        
                    </tr>
                    @endforeach
                </tbody>
            </table>
            </div>
          <!-- Table End -->
          
          </div>
        </div>  
       </div> 
    </div>
</div>



<script>
  $(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    /*  When user click add user button */
    $('#create-new-invoice').click(function () {        
      $('#btn-save').val("create-Invoice");
      $('#userForm').trigger("reset");
      $('#taskModal').html("Create Invoice");
      $('#invoiceID').fadeOut();
      $('#customerPayName').fadeOut();
      $('#assigneeTag').fadeIn();
      $('#Description').fadeIn(); 
      $('#task-modal').modal('show');

    });
    /*  End When user click add user button */

    /*  When user click add invoiceline button */
    $('body').on('click', '#create-new-invoiceLine', function () {

      var invoice_id = $(this).data('id');
      var old_amount = $(this).data('name');
      $('#btn-save').val("create-InvoiceLine");
      $('#userForm').trigger("reset");
      $('#taskModal').html("Create InvoiceLine");
      $('#assigneeTag').fadeOut();
      $('#customerPayName').fadeOut();
      $('#invoiceID').fadeIn();
      $('#Description').fadeIn();        
      $('#task-modal').modal('show');
      $('#invoiceid').val(invoice_id);
      $('#oldamount').val(old_amount);
        
    }); 
    /*End When user click add invoiceline button */

    /*  When user click make-payment button */
    $('body').on('click', '#make-payment', function () {

      var customer_pay_id = $(this).data('id');
      var customer_name = $(this).data('name');
      $('#btn-save').val("create-Payment");
      $('#userForm').trigger("reset");
      $('#taskModal').html("Make Payment");

      $('#assigneeTag').fadeOut();
      $('#invoiceID').fadeOut();
      $('#Description').fadeOut();
      $('#customerPayName').fadeIn();

      $('#task-modal').modal('show');
      $('#customername').val(customer_name);
      $('#oldamount').val(customer_pay_id);

    }); 
    /*End When user click make-payment button*/ 


  });
 
if  ($("#userForm").length > 0) {
      $("#userForm").validate({
  
        submitHandler: function(form) {
      
          var actionType = $('#btn-save').val();
          $('#btn-save').html('Sending..');
          
          $.ajax({
              data: $('#userForm').serialize(),
              url: "{{ url('invoices')}}", 
              type: "POST",
              dataType: 'json',
              success: function (data) {

                  console.log(data);
                  var user = '<tr id="task_id_' + data.id + '"><td>' + data.id + '</td><td>' + data.customer_id + '</td><td>' + data.description + '</td><td>' + data.created_at + '</td><td>' + data.amount + '</td>';
                      user += '<td><a href="javascript:void(0)" id="create-new-invoiceLine" data-id="' + data.id + '" class="btn btn-success toChat">Add</a></td>';
                      user += '<td><a href="javascript:void(0)" id="make-payment" data-id="' + data.id + '" class="btn btn-info toEdit">Pay</a></td></tr>';
                
                  if (data == "Payment Successfull"){  
                    alert(data);
                  }  

                  if (data == "Insuficient Funds"){       
                    alert(data);
                  }  
                  
                  
                  if (actionType == "create-Invoice") {
                      location.reload(); 
                      // $('#users-crud').prepend(user);
                  } 
                  else if(actionType == "create-InvoiceLine"){
                      $("#task_id_" + data.id).replaceWith(user);
                  }
      
                  $('#userForm').trigger("reset");
                  $('#task-modal').modal('hide');
                  $('#btn-save').html('Create');
                  
            },
            error: function (data) {
                console.log('Error:', data);
                $('#btn-save').html('Save Changes');
            }
            
          });
        }
      })
    }
   
  
</script>
    
@endsection