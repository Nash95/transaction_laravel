@extends('layouts.admin')

@section('content')
    
<div class="container-fluid">
    <h2 style="margin-top: 12px;" class="alert alert-success">Customers</h2><br>
    <div class="row">
        <div class="col-12">
          <a href="javascript:void(0)" class="btn btn-success mb-2" id="create-new-user">Add Customer</a> 
          <table class="table table-bordered" id="laravel_crud">
           <thead>
              <tr>
                 <th>Id</th>
                 <th>Name</th>
                 <th>Address</th>
                 <th>Join Date</th>
                 <th>Balance</th>
                 
                 <td colspan="2">Action</td>
              </tr>
           </thead>
           <tbody id="customers-crud">
              @foreach($customers as $u_info)
              <tr id="user_id_{{ $u_info->id }}">
                 <td>{{ $u_info->id  }}</td>
                 <td>{{ $u_info->name }}</td>
                 <td>{{ $u_info->address }}</td>
                 <td>{{ $u_info->created_at }}</td>
                 <td><b>R</b>{{ $u_info->balance }}</td>
                 <td colspan="2">
                    <a href="javascript:void(0)" id="edit-user" data-id="{{ $u_info->id }}" class="btn btn-info mr-2">Update</a>
                    <a href="javascript:void(0)" id="delete-user" data-id="{{ $u_info->id }}" class="btn btn-danger delete-user">Delete</a>
                  </td>
              </tr>
              @endforeach
           </tbody>
          </table>
       </div> 
    </div>
</div>

<div class="modal fade" id="transact-modal" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title" id="crudModal"></h4>
        </div>
        <form id="userForm" name="userForm" class="form-horizontal">
          <div class="modal-body">
              <input type="hidden" name="user_id" id="user_id">
              <div class="form-group">
                <label for="name" class="col-sm-2 control-label">Name</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control" id="name" name="name" placeholder="Enter Name" value="" maxlength="50" required="">
                </div>
              </div>
 
              <div class="form-group">
              <label class="col-sm-2 control-label">Address</label>
                <div class="col-sm-12">
                    <input type="name" class="form-control" id="address" name="address" placeholder="Enter Address" value="" required="">
                </div>
              </div>

              <div class="form-group">
              <label class="col-sm-2 control-label">Username</label>
                <div class="col-sm-12">
                    <input type="name" class="form-control" id="username" name="username" placeholder="Enter Username" value="" required="">
                </div>
              </div>

              <div class="form-group">
              <label class="col-sm-2 control-label">Password</label>
                <div class="col-sm-12">
                    <input type="password" class="form-control" id="password" name="password" placeholder="Enter Password" value="" required="">
                </div>
              </div>

              <div class="form-group">
              <label class="col-sm-2 control-label">Balance</label>
                <div class="col-sm-12">
                    <input type="name" class="form-control" id="balance" name="balance" placeholder="Enter Balance" value="" required="">
                </div>
              </div>

          </div>
          <div class="modal-footer">
              <button type="submit" class="btn btn-primary" id="btn-save" value="create">
                Save changes
              </button>
          </div>
        </form>
    </div>
  </div>
</div>

<script>
  $(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    /*  When user click add user button */
    $('#create-new-user').click(function () {
        $('#btn-save').val("create-customer");
        $('#userForm').trigger("reset");
        $('#crudModal').html("Add New Customer");
        $('#transact-modal').modal('show');
    });
 
   /* When click edit user */
   $('body').on('click', '#edit-user', function () {
        // console.log ("madii");
      var user_id = $(this).data('id');
      console.log("the user id is"+user_id);
      $.get('customers/' + user_id +'/edit', function (data) {
         $('#crudModal').html("Edit Customer");
          $('#btn-save').val("edit-user");
          $('#transact-modal').modal('show');
          $('#user_id').val(data.id);
          $('#name').val(data.name);
          $('#address').val(data.address);
          $('#username').val(data.username);
          $('#password').val(data.password);
          $('#balance').val(data.balance);
      })
   });
   //delete user login
    $('body').on('click', '.delete-user', function () {
        var user_id = $(this).data("id");
        if(confirm("Are You sure want to delete !")) {
 
        $.ajax({
            type: "DELETE",
            url: "{{ url('customers')}}"+'/'+user_id,
            success: function (data) {
                $("#user_id_" + user_id).remove();
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
       }
    });   
  });
 
 if ($("#userForm").length > 0) {
      $("#userForm").validate({
 
     submitHandler: function(form) {
 
      var actionType = $('#btn-save').val();
      $('#btn-save').html('Sending..');
      
      $.ajax({
          data: $('#userForm').serialize(),
          url: "{{ url('customers')}}",
          type: "POST",
          dataType: 'json',
          success: function (data) {
              var user = '<tr id="user_id_' + data.id + '"><td>' + data.id + '</td><td>' + data.name + '</td><td>' + data.address+ '</td><td>' + data.created_at+ '</td><td> <b>R</b>' + data.balance + '</td>';
              user += '<td colspan="2"><a href="javascript:void(0)" id="edit-user" data-id="' + data.id + '" class="btn btn-info mr-2">Update</a>';
                user += '<a href="javascript:void(0)" id="delete-user" data-id="' + data.id + '" class="btn btn-danger delete-user ml-1">Delete</a></td></tr>';
               
              
              if (actionType == "create-customer") {
                  $('#customers-crud').prepend(user);
              } else {
                  $("#user_id_" + data.id).replaceWith(user);
              }
 
              $('#userForm').trigger("reset");
              $('#transact-modal').modal('hide');
              $('#btn-save').html('Save Changes');
              
          },
          error: function (data) {
              console.log('Error:', data);
              $('#btn-save').html('Save Changes');
          }
      });
    }
  })
}
   
  
</script>
    
@endsection