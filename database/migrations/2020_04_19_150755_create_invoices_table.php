<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoicesTable extends Migration
{
    
    
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('customer_id');      
            $table->text('description');
            $table->timestamp('created_at')->useCurrent();
            $table->float('amount');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('invoices');
    }

    
    
}