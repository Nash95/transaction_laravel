<table class="table table-head-fixed text-nowrap" id="laravel_crud">
    <thead>
        <tr>
            <th>Id</th>
            <th>Customer</th>
            <th>Description</th>
            <th>Date Created</th>
            <th>Amount</th>
            <th>Add InvoiceLine</th>
            <th>Make Payment</th>
            
        </tr>
    </thead>
    <tbody id="users-crud">
        @foreach($invoices as $t_info)
        <tr >
            <td>{{ $t_info->id  }}</td>
            <td>{{ $t_info->name }}</td>
            <td>{{ $t_info->description }}</td>
            <td>{{ $t_info->created_at }}</td>
            <td>{{ $t_info->amount }}</td>
            <td>
                <a href="javascript:void(0)" data-name="{{ $t_info->amount }}" id="create-new-invoiceLine" data-id="{{ $t_info->id }}" class="btn btn-success">Add</a>                    
            </td>
            <td>
                <a href="javascript:void(0)" data-name="{{ $t_info->name }}" id="make-payment" data-id="{{ $t_info->customer_id }}" class="btn btn-info">Pay</a>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>