<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $fillable = [
        'customer_id','created_at', 'amount'
    ];

    public function getUpdatedAtColumn() {
        return null;
    }
}
