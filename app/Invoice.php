<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $fillable = [
        'customer_id','description','created_at', 'amount'
    ];

    public function getUpdatedAtColumn() {
        return null;
    }
}
